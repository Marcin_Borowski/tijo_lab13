package pl.edu.pwsztar;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class ShoppingCartTest {
    private ShoppingCart shopcart;
    @BeforeEach
    void prepare(){
                shopcart = new ShoppingCart();
                shopcart.addProducts("Item 1",25,20);
                shopcart.addProducts("Item 2",12,4);
                shopcart.addProducts("Item 4",6,6);
            }
    @ParameterizedTest(name = "should be able to add item {0} of price {1} and quantity {2}")
    @CsvSource({
            "Item 1,25,20",
            "Item 2,12,5",
            "Item 3,13,3",
            "Item 4,6,3",
    })

    void normalAddingTest(String itemName,int price, int quantity){
        assertTrue(shopcart.addProducts(itemName,price,quantity));
    }

    @ParameterizedTest(name = "Adding item {0} of price {1} should return {3}")
    @CsvSource({
            "Item 1,3,20",
            "Item 4,3,20",
            "Item 2,4,5",
            "Item 4,1,5",
            "Item 4,6,500",
    })

    void FailedAddingTest(String itemName,int price,int quantity){
        assertFalse(shopcart.addProducts(itemName,price,quantity));
    }

    @ParameterizedTest(name = "Should Delete {1} of {0}")
    @CsvSource({
            "Item 1,2",
            "Item 2,1",
            "Item 4,4",
    })

    void deletingOkItems(String itemName,int quantity){
        assertTrue(shopcart.deleteProducts(itemName,quantity));
    }

    @ParameterizedTest(name = "Should not be able to delete {1} of {0}")
    @CsvSource({
            "Item 1,0",
            "Item 2,-3",
            "Item 3,1",
            "item 4,100",
    })

    void deletingWrongItems(String itemName,int quantity){
        assertFalse(shopcart.deleteProducts(itemName,quantity));
    }

    @ParameterizedTest(name = "Should return {3} of {0}")
    @CsvSource({
            "Item 1,25,10,30",
            "Item 2,12,7,11",
            "Item 4,6,10,16",
    })

    void gettingQuantityTest(String itemName,int price, int quantity, int expected){
        shopcart.addProducts(itemName,price,quantity);
        assertEquals(expected,shopcart.getQuantityOfProduct(itemName));
    }

    @Test
    @DisplayName("[Getting Total Price] Should return total price of 584")

    void gettingSumPricesTest(){
        assertEquals(584,shopcart.getSumProductsPrices());
    }



    @ParameterizedTest(name = "Should return {1} for {0}")
    @CsvSource({
            "Item 1,25",
            "Item 2,12",
    })

    void getProductPriceTest(String itemName,int expected){
        assertEquals(expected,shopcart.getProductPrice(itemName));
    }



    @Test
    @DisplayName("Should Return List of products")

    void getProductsNamesTest(){
        List<String> expected = Arrays.asList("Item 1","Item 2","Item 4");
        assertEquals(expected,shopcart.getProductsNames());
    }
}

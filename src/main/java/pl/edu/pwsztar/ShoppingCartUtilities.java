package pl.edu.pwsztar;

import java.util.List;

public class ShoppingCartUtilities {

    public static int findItem(String itemName, List<ShoppingCartItem> shopcart){
        int position = 0;

        for(ShoppingCartItem item: shopcart){
            if(item.getProductName().equals(itemName)){
                return position;
            }
            position++;
        }
        return -1;
    }

    public static int countProducts(String itemName, List<ShoppingCartItem> shopcart){
    for (ShoppingCartItem item: shopcart){
        if(item.getProductName().equals(itemName)){
            return item.getQuantity();
            }
    }
    return 0;
}

    public static int countAllProducts(List<ShoppingCartItem> shopcart){
        return shopcart.stream().mapToInt(ShoppingCartItem::getQuantity).sum();
    }
}

package pl.edu.pwsztar;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShoppingCart implements ShoppingCartOperation {

    List<ShoppingCartItem> shopcartContents = new ArrayList<>();

    public boolean addProducts(String productName, int price, int quantity) {
        if(quantity<1){
            return false;
        }
        if(ShoppingCartUtilities.countAllProducts(shopcartContents)+quantity>PRODUCTS_LIMIT){
            return false;
        }

        int found = ShoppingCartUtilities.findItem(productName,shopcartContents);

        if(found != -1){
            if(shopcartContents.get(found).getPrice()!=price){
                return false;
            } else {
                shopcartContents.get(found).setQuantity(shopcartContents.get(found).getQuantity()+quantity);
                return true;
            }
        } else {
            shopcartContents.add(new ShoppingCartItem(productName,price,quantity));
            return true;
        }
    }

    public boolean deleteProducts(String productName, int quantity) {
        if(quantity<1){
            return false;
        }
        int found = ShoppingCartUtilities.findItem(productName,shopcartContents);
        if(found == -1){
            return false;
        }
            shopcartContents.get(found).setQuantity(shopcartContents.get(found).getQuantity()-quantity);
        if(shopcartContents.get(found).getQuantity()<1){
            shopcartContents.remove(found);
        }
        return true;
    }

    public int getQuantityOfProduct(String productName) {
        return ShoppingCartUtilities.countProducts(productName,shopcartContents);
    }

    public int getSumProductsPrices() {
        return shopcartContents.stream().mapToInt(item -> item.getPrice()*item.getQuantity()).sum();
    }

    public int getProductPrice(String productName) {
        int pos = ShoppingCartUtilities.findItem(productName,shopcartContents);
        if (pos==-1) {
            return 0;
        }
        return shopcartContents.get(pos).getPrice();
    }

    public List<String> getProductsNames() {
        return shopcartContents.stream().map(ShoppingCartItem::getProductName).collect(Collectors.toList());
    }
}
